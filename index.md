![AI generated header image showing fields and mountains](header.png)

# Markdown to Html Cli Pages Template

This is a template for an as-simple-as-possible static website using the [Markdown-to-Html CLI](https://wangchujiang.com/markdown-to-html-cli/) in conjunction with Gitlab's CI/CD as a "static site generator".

## Getting started

To see your website with your own gitlab.io URL before changing anything, just run a pipeline on the `main` branch of the project you created from this template.

Simply change the content of this index.md file. You can use a markdown preview to get an idea of how the website will look like. Just commit and push to the main branch, and your website will be live in just a few minutes.

## Adding more pages

By default, this page will be available on the root (`example.com`) and `/index` routes. You can add other pages by simply creating more `.md` files, e.g. `contact.md` to show some contact information. These will get converted to html files and added to the output, so your `contect.md` would be available at `example.com/contact.html`, as any markdown files get converted to html. This also means you can add links to other pages by adding a link to your markdown file like `[Contact information](contact.html)`.

## Change or add styling

To simply change the width of your website, you can fiddle with the values in `.gitlab-ci.md`. I tried setting sensible defaults here to make it work on both desktop and phone screens, but there's always room for improvement I guess.

As markdown is a superset of html, you can either write html directly in there (including a style attribute on a tag, or a CSS tag), or create a css file and reference it from your markdown file like `<link rel="stylesheet" type="text/css" href="style.css">`. 

## Images or files to download

You can add images to your website by adding them to the root directory here and referencing them in your markdown like `![alternate text](your-image.png)`.

You can also add other kinds of files to the root directory and just link them, in order to offer them as download like `[download excel file here](my-table-calc.xlsx)`.